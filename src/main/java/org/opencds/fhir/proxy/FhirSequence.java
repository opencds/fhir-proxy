/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy;

import ca.uhn.fhir.context.FhirVersionEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum FhirSequence {
    DSTU2(FhirVersionEnum.DSTU2_HL7ORG),
    DSTU3(FhirVersionEnum.DSTU3),
    STU3(FhirVersionEnum.DSTU3),
    R4(FhirVersionEnum.R4),
    UNKNOWN(null);

    private static final Log log = LogFactory.getLog(FhirSequence.class);

    private final FhirVersionEnum fv;

    FhirSequence(FhirVersionEnum fv) {
        this.fv = fv;
    }

    public FhirVersionEnum getFhirVersionEnum() {
        return fv;
    }

    public static FhirSequence resolve(String stu) {
        FhirSequence fhirSequence = FhirSequence.UNKNOWN;
        if (stu != null) {
            try {
                fhirSequence = FhirSequence.valueOf(stu.toUpperCase());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return fhirSequence;
    }
}
