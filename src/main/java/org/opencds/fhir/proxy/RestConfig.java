/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy;

import ca.uhn.fhir.context.FhirVersionEnum;
import org.opencds.fhir.proxy.service.FhirRequestHandler;
import org.opencds.fhir.proxy.service.dstu2.Dstu2RequestHandler;
import org.opencds.fhir.proxy.service.r4.R4RequestHandler;
import org.opencds.fhir.proxy.service.stu3.Stu3RequestHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Configuration
@ApplicationPath("/resources")
public class RestConfig extends Application {
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(
                Arrays.asList(FhirMetadataProxy.class, FhirProxy.class, CorsFilter.class));
    }

    private static final String URL_DSTU2 = "http://hapi.fhir.org/baseDstu2/";
    private static final String URL_STU3 = "http://hapi.fhir.org/baseDstu3/";
    private static final String URL_R4 = "http://hapi.fhir.org/baseR4/";

    @Bean
    public Map<FhirVersionEnum, FhirRequestHandler> fhirSequenceRequestHandlerMap() {
        Map<FhirVersionEnum, FhirRequestHandler> map = new HashMap<>();
        map.put(FhirVersionEnum.DSTU2_HL7ORG, new Dstu2RequestHandler(URL_DSTU2));
        map.put(FhirVersionEnum.DSTU3, new Stu3RequestHandler(URL_STU3));
        map.put(FhirVersionEnum.R4, new R4RequestHandler(URL_R4));
        return map;
    }

}