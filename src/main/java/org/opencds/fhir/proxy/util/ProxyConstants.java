package org.opencds.fhir.proxy.util;

public class ProxyConstants {
    public static final String XML = "xml";
    public static final String SMART_OAUTH_URIS = "http://fhir-registry.smarthealthit.org/StructureDefinition/oauth-uris";
    public static final String HL7_RESTFUL_SEC_SVC = "http://hl7.org/fhir/restful-security-service";
    public static final String OAUTH = "OAuth";
    public static final String SMART_ON_FHIR = "SMART-on-FHIR";

}
