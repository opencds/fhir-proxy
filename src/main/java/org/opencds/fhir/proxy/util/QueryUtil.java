package org.opencds.fhir.proxy.util;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryUtil {

    public static Map<String, List<String>> parseQuery(String queryString) {
        if (queryString == null) {
            return Collections.emptyMap();
        }
        return Arrays.stream(queryString.split("&"))
                .map(qp -> {
                    String[] qv = qp.split("=");
                    return new AbstractMap.SimpleEntry<>(qv[0], (qv.length == 2 ? qv[1] : ""));
                })
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.mapping(Map.Entry::getValue, Collectors.toList())
                        ));
    }

}
