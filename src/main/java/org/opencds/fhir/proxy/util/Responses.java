package org.opencds.fhir.proxy.util;

import com.google.common.collect.ImmutableMap;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Responses {
    private static Set<String> methods = new HashSet<>(
            Arrays.asList("POST", "GET", "OPTIONS", "HEAD"));
    
    private static final Map<String, String> CORS_HEADERS = ImmutableMap.of(
    		"Access-Control-Allow-Origin", "*", 
    		"Access-Control-Allow-Headers", "origin, content-type, accept, authorization",
    		"Access-Control-Allow-Credentials", "true",
    		"Access-Control-Allow-Methods", "GET, POST, OPTIONS, HEAD");
    
    private static final Map<String, String> NO_CACHE_HEADERS = ImmutableMap.of(
    		"Cache-Control", "no-cache, no-store, must-revalidate", 
    		"Pragma", "no-cache",
    		"Expires", "0");

    // 200
    public static Response ok() {
        return Response.ok().build();
    }

    // 200
    public static Response ok(String data) {
        return Response.ok().entity(data).build();
    }
        
    public static Response ok(Map<String, String> headers, String data) {
        Response.ResponseBuilder response = Response.ok();
        headers.forEach(response::header);
        return response.entity(data).build();
    }

    public static Response ok(Map<String, String> headers, InputStream data) {
        Response.ResponseBuilder response = Response.ok();
        headers.forEach(response::header);
        return response.entity(data).build();
    }

    // 201
    public static Response created(UriInfo uriInfo) {
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    // 201
    public static Response created(UriInfo uriInfo, String uriSegment) {
        return Response.created(
                uriInfo.getAbsolutePathBuilder().path(uriSegment).build())
                .build();
    }

    // 204
    public static Response noContent() {
        return Response.noContent().build();
    }
    
    // 302
    public static Response found(String location) {
        return Response.status(Status.FOUND).header("Location", location).build();
    }
    
    // 302
    public static Response found(URI location) {
        return found(location.toString());
    }
    
    // 400
    public static Response badRequest(String message) {
        return Response.status(Status.BAD_REQUEST).entity(message)
                .allow(methods).build();
    }
    
    // 401
    public static Response unauthorized(String message) {
        return Response.status(Status.UNAUTHORIZED).entity(message)
                .allow(methods).build();
    }

    // 404
    public static Response notFound() {
        return Response.status(Status.NOT_FOUND).allow(methods).build();
    }

    // 404
    public static Response notFound(String message) {
        return Response.status(Status.NOT_FOUND).entity(message).allow(methods)
                .build();
    }

    // 409
    public static Response conflict(String message) {
        return Response.status(Status.CONFLICT).entity(message).allow(methods)
                .build();
    }

    // 500
    public static Response internalServerError() {
        return Response.status(Status.INTERNAL_SERVER_ERROR).allow(methods)
                .build();
    }

    // 500
    public static Response internalServerError(String message) {
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(message)
                .allow(methods).build();
    }

    public static Response resolve(Exception e) {
        Status status = Status.INTERNAL_SERVER_ERROR;
        for (Status st : Status.values()) {
            if (st.getReasonPhrase().equalsIgnoreCase(e.getMessage())) {
                status = st;
                break;
            }
        }
        return Response.status(status).allow(methods)
                .build();
    }

}
