/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
@EnableAuthorizationServer
public class FhirAuthorizationServer extends AuthorizationServerConfigurerAdapter {


    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients()
        ;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                .inMemory()
                .withClient("pccds-client")
                .secret(passwordEncoder.encode(""))
                .authorizedGrantTypes("authorization_code")
                .authorities("CLIENT")
                .scopes("resources:read")
                .resourceIds("fhir-resources")
                .redirectUris("https://fhir.cygni.cc/pccds/redirect", "http://localhost:4545/pccds/redirect")
                .accessTokenValiditySeconds(3600)
        ;

    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenEnhancer(tokenEnhancer());
    }

    private TokenEnhancer tokenEnhancer() {
        return (accessToken, authentication) -> {
            DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken);
            Map<String, Object> additionalInfo = new LinkedHashMap<>();
//            additionalInfo.put("patient", "15033"); // stu3 pt from hapi-fhir test server
            additionalInfo.put("patient", "642"); // dstu2 pt from hapi-fhir test server
            additionalInfo.put("user", getUser(authentication.getPrincipal()));
            token.setAdditionalInformation(additionalInfo);
            return token;
        };
    }

    private String getUser(Object principal) {
        if (principal instanceof User) {
            return ((User)principal).getUsername();
        }
        return null;
    }

}
