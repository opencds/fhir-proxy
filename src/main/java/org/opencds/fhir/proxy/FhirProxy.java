/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.api.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.fhir.proxy.service.FhirRequestHandler;
import org.opencds.fhir.proxy.util.QueryUtil;
import org.opencds.fhir.proxy.util.Responses;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/{fhirSequence}")
public class FhirProxy {
    private static final Log log = LogFactory.getLog(FhirProxy.class);
    private static final String METADATA = "metadata";
    private final Map<FhirVersionEnum, FhirRequestHandler> handlerMap;

    @Autowired
    public FhirProxy(Map<FhirVersionEnum, FhirRequestHandler> fhirSequenceRequestHandlerMap) {
        this.handlerMap = fhirSequenceRequestHandlerMap;
    }

    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, Constants.CT_FHIR_JSON, Constants.CT_FHIR_XML })
    @GET
    @Path("/{resource}/{path : .+}")
    public Response read(@PathParam("fhirSequence") String fhirSequence, @PathParam("resource") String resource, @PathParam("path") String path, @Context HttpServletRequest req) {
        FhirSequence sequence = FhirSequence.resolve(fhirSequence);
        FhirRequestHandler handler = handlerMap.get(sequence.getFhirVersionEnum());
        if (sequence == FhirSequence.UNKNOWN) {
            return Responses.notFound();
        }

        String accept = req.getHeader(Constants.HEADER_ACCEPT);
        Map<String, List<String>> query = QueryUtil.parseQuery(req.getQueryString());

        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.HEADER_CONTENT_TYPE, accept.contains("*/*") ? Constants.CT_JSON : accept);
        return Responses.ok(headers, handler.read(resource, path, query, accept));
    }

    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, Constants.CT_FHIR_JSON, Constants.CT_FHIR_XML })
    @GET
    @Path("{resource}")
    public Response search(@PathParam("fhirSequence") String fhirSequence, @PathParam("resource") String resource, @Context HttpServletRequest req) {
        FhirSequence sequence = FhirSequence.resolve(fhirSequence);
        FhirRequestHandler handler = handlerMap.get(sequence.getFhirVersionEnum());
        if (sequence == FhirSequence.UNKNOWN) {
            return Responses.notFound();
        }

        String accept = req.getHeader(Constants.HEADER_ACCEPT);
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.HEADER_CONTENT_TYPE, accept.equals("*/*") ? Constants.CT_JSON : accept);
        String authHeader = req.getHeader(Constants.HEADER_AUTHORIZATION);
        return Responses.ok(headers, handler.read(resource, null, QueryUtil.parseQuery(req.getQueryString()), accept));
    }

}

