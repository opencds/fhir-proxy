/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.api.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.fhir.proxy.service.FhirRequestHandler;
import org.opencds.fhir.proxy.util.Responses;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Path("/{fhirSequence}/metadata")
public class FhirMetadataProxy {
    private static final Log log = LogFactory.getLog(FhirMetadataProxy.class);
    private static final String METADATA = "metadata";
    private final Map<FhirVersionEnum, FhirRequestHandler> handlerMap;

    @Autowired
    public FhirMetadataProxy(Map<FhirVersionEnum, FhirRequestHandler> fhirSequenceRequestHandlerMap) {
        this.handlerMap = fhirSequenceRequestHandlerMap;
    }

    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, Constants.CT_FHIR_JSON, Constants.CT_FHIR_XML })
    @GET
    public Response metadata(@PathParam("fhirSequence") String fhirSequence, @Context HttpServletRequest req, @Context UriInfo uriInfo) {
        FhirSequence sequence = FhirSequence.resolve(fhirSequence);
        FhirRequestHandler handler = handlerMap.get(sequence.getFhirVersionEnum());
        if (sequence == FhirSequence.UNKNOWN) {
            return Responses.notFound();
        }

        String accept = req.getHeader(Constants.HEADER_ACCEPT);
        Map<String, String> headers = new HashMap<>();
        headers.put(Constants.HEADER_CONTENT_TYPE, accept.contains("*/*") ? Constants.CT_JSON : accept);
        return Responses.ok(headers, handler.getMetadata(getBaseUriWithoutApi(uriInfo.getBaseUri()), accept));
    }

    private String getBaseUriWithoutApi(URI uri) {
        return uri.toString().replaceFirst("api/", "");
    }

}

