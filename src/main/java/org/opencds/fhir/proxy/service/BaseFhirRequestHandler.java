/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class BaseFhirRequestHandler implements FhirRequestHandler {

    private final String baseUrl;
    private WebTarget webTarget;

    protected final FhirContext ctx;
    protected final IGenericClient hapiClient;


    protected BaseFhirRequestHandler(String baseUrl, FhirContext ctx) {
        this.baseUrl = baseUrl;
        this.ctx = ctx;
        hapiClient = ctx.newRestfulGenericClient(baseUrl);
        webTarget = ClientBuilder.newBuilder()
                .property(ClientProperties.JSON_PROCESSING_FEATURE_DISABLE, true)
                .build()
                .target(formatBaseUrl(baseUrl));
    }

    @Override
    public WebTarget getWebTarget() {
        return webTarget;
    }

    private String formatBaseUrl(String baseUrl) {
        return baseUrl.endsWith("/") ? baseUrl : baseUrl + "/";
    }

}
