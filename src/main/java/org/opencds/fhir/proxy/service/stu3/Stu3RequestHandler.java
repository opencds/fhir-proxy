/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy.service.stu3;

import ca.uhn.fhir.context.FhirContext;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.dstu3.model.CapabilityStatement;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.UriType;
import org.opencds.fhir.proxy.service.BaseFhirRequestHandler;
import org.opencds.fhir.proxy.util.ProxyConstants;

public class Stu3RequestHandler extends BaseFhirRequestHandler {
    public Stu3RequestHandler(String baseUrl) {
        super(baseUrl, FhirContext.forDstu3());
    }

    @Override
    public String getMetadata(String baseUri, String accept) {
        CapabilityStatement cs = hapiClient.capabilities().ofType(CapabilityStatement.class).execute();
        Extension oauth = new Extension().setUrl(ProxyConstants.SMART_OAUTH_URIS);
        oauth.addExtension(new Extension()
                .setUrl("authorize")
                .setValue(new UriType(baseUri + "oauth/authorize")));
        oauth.addExtension(new Extension()
                .setUrl("token")
                .setValue(new UriType(baseUri + "oauth/token")));

         cs.getRest().get(0)
                 .getSecurity()
                 .setCors(true)
                 .addService(new CodeableConcept()
                         .addCoding(new Coding()
                                 .setSystem(ProxyConstants.HL7_RESTFUL_SEC_SVC)
                                 .setCode(ProxyConstants.OAUTH)))
                 .addService(new CodeableConcept()
                         .addCoding(new Coding()
                                 .setSystem(ProxyConstants.HL7_RESTFUL_SEC_SVC)
                                 .setCode(ProxyConstants.SMART_ON_FHIR)))
                 .addExtension(oauth);

        if (StringUtils.containsIgnoreCase(accept, ProxyConstants.XML)) {
            return ctx.newXmlParser().encodeResourceToString(cs);
        }

        return ctx.newJsonParser().encodeResourceToString(cs);
    }

}
