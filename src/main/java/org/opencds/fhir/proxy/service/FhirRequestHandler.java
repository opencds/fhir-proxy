/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy.service;

import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface FhirRequestHandler {
    static final byte[] EMPTY = new byte[]{};

    String getMetadata(String baseUri, String accept);

    default InputStream read(String resource, String path, Map<String, List<String>> query, String accept) {
        WebTarget target = getWebTarget();
        if (path == null) {
            target = target.path(resource);
        } else {
            target = target.path(resource + "/" + path);
        }
        for (Map.Entry<String, List<String>> entry : query.entrySet()) {
            target = target.queryParam(
                    entry.getKey(),
                    entry.getValue().toArray());
        }
        Response response = target.request()
                .accept(StringUtils.isNotBlank(accept) ? accept : MediaType.APPLICATION_JSON)
                .get();
        if (response.hasEntity()) {
            return (InputStream) response.getEntity();
        }
        return new ByteArrayInputStream(EMPTY);
    }

    WebTarget getWebTarget();
}
