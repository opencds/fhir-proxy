/*
Copyright 2020 OpenCDS.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package org.opencds.fhir.proxy.util;

import spock.lang.Specification
import spock.lang.Unroll;

import java.util.Map;

public class QueryUtilSpec extends Specification {

    @Unroll
    def 'test '() {
        expect:
        result == QueryUtil.parseQuery(query)

        where:
        query | result
        'one=two' | ['one' : ['two']]
        'one=two&one=three' | ['one':['two', 'three']]
        'one=two&three=four' | ['one': ['two'], 'three': ['four']]
        'one=two&three=four&three=five' | ['one': ['two'], 'three': ['four', 'five']]
    }

    def 'test empty'() {
        expect:
        QueryUtil.parseQuery('') instanceof Map<String, List<String>>
    }

    def 'test null'() {
        expect:
        QueryUtil.parseQuery(null) instanceof Map<String, List<String>>
    }
}
