# fhir-proxy

fhir-proxy is a simple implementation of a [FHIR](http://fhir.hl7.org) server with the addition of an
[OAuth 2.0](https://tools.ietf.org/html/rfc6749) authorization server.  Specifically, the server
conforms to a basic implementation of a [SMART on FHIR](https://docs.smarthealthit.org) service,
providing the [SMART App Launch](http://www.hl7.org/fhir/smart-app-launch/) capability.

This project was developed for the [PCCDS](https://pccds-ln.org) Proof of Concept found here:
[pccds-poc](https://bitbucket.org/euvitudo/pccds-poc), for the
[2019 Annual PCCDS Conference](https://pccds-ln.org/2019conference).

The server does not implement a storage backend, but provides basic FHIR [Read](http://hl7.org/fhir/http.html#read)
and [Search](http://hl7.org/fhir/search.html) capabilities.  A backing FHIR server **is required**.  The base
server is configured to use the HAPI-FHIR service available at [http://hapi.fhir.org](http://hapi.fhir.org).
A client may use either FHIR [DSTU2](http://hl7.org/fhir/DSTU2), [STU3](http://hl7.org/fhir/STU3) or
[R4](http://hl7.org/fhir/R4).

The FHIR endpoints are as follows:
* `http://localhost:8080/fhir-proxy/api/dstu2`
    * Points to `http://hapi.fhir.org/baseDstu2`
* `http://localhost:8080/fhir-proxy/api/stu3` or `http://localhost:8080/fhir-proxy/api/dstu3`
    * Points to `http://hapi.fhir.org/baseDstu3`
* `http://localhost:8080/fhir-proxy/api/r4`
    * Points to `http://hapi.fhir.org/baseR4`

The metadata at each endpoint contains reference to the `authorization` and `token` endpoints.

### Note:
* The endpoints noted above are currently hard-coded in the
`org.opencds.fhir.proxy.RestConfig` class.

* OAuth 2.0 clients are currently configured (also hard-coded) in the
`org.opencds.fhir.proxy.oauth.FhirAuthorizationServer` class.
    * Currently only one client is configured.

* The user details is currently configured (also hard-coded) in the
`org.opencds.fhir.proxy.oauth.OAuthConfig` class.
    * Currently only one user details is configured.

### How do I get set up? ###

The project can be built with maven, e.g.,

```mvn clean install```

The `war` may be deployed to any Java servlet container.

The project may also be run from any IDE with the appropriate IDE configuration.

## License

This project is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).